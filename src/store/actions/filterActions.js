import { UPDATE_FILTER_ACTION } from "../reducers/filterReducer.js";

export const filterActions = (value) => ({
    type: UPDATE_FILTER_ACTION,
    payload: value
});
