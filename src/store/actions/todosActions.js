import {
    ADD_TODO_ACTION,
    UPDATE_TODO_ACTION,
    DELETE_TODO_ACTION,
    DELETE_CLEAR_COMPLETED_ACTION
} from '../reducers/todosReducer';

export const addTodoAction = (title) => ({
    type: ADD_TODO_ACTION,
    payload: {title}
});

export const updateTodoAction = (todo) => ({
    type: UPDATE_TODO_ACTION,
    payload: {
        ...todo,
        completed: !todo.completed
    }
});

export const deleteTodoAction = (todo) => ({
    type: DELETE_TODO_ACTION,
    payload: todo.id
});

export const deleteClearCompletedAction = () => ({
    type: DELETE_CLEAR_COMPLETED_ACTION,
});
