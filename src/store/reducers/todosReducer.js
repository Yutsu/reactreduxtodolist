let id = 6;
const initialState = [
    {
        id: 0,
        title: "Complete online Javascript course",
        completed: true
    },
    {
        id: 1,
        title: "Jog around the park 3x",
        completed: false,
    },
    {
        id: 2,
        title: "10 minutes meditation",
        completed: false
    },
    {
        id: 3,
        title: "Read for 1 hour",
        completed: false
    },
    {
        id: 4,
        title: "Pick up groceries",
        completed: false
    },
    {
        id: 5,
        title: "Complete Todo App on Frontend Mentor",
        completed: false
    }
];

export const ADD_TODO_ACTION = "ADD_TODO_ACTION";
export const UPDATE_TODO_ACTION = "UPDATE_TODO_ACTION";
export const DELETE_TODO_ACTION = "DELETE_TODO_ACTION";
export const DELETE_CLEAR_COMPLETED_ACTION = "DELETE_CLEAR_COMPLETED_ACTION";

export const todosReducer = (state = initialState, action) => {
    switch(action.type){
        case ADD_TODO_ACTION:
            console.log(id)
            return [
                ...state,
                {
                    id: id++,
                    ...action.payload,
                    completed: false
                }
            ];

        case UPDATE_TODO_ACTION:
            return state.map(todo => {
                if(todo.id === action.payload.id){
                    return {
                        ...todo,
                        ...action.payload
                    }
                }
                else {
                    return todo;
                }
            });

        case DELETE_TODO_ACTION:
            return state.filter(todo => (
                todo.id !== action.payload
            ));

        case DELETE_CLEAR_COMPLETED_ACTION:
            return state.filter(todo => (
                todo.completed === false
            ));

        default:
            return state;
    }
};

export default todosReducer;