export const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('todo');
        if(serializedState === null){
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch(err) {
        return console.log(err);
    }
}

export const saveToLocalStorage = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('todo', serializedState)
    } catch (error) {
        console.log(error);
    }
}