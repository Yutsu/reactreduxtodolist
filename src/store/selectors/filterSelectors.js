export const filterSelectors = ({todos, filter}) => {
    if(filter === null){
        return todos;
    }
    return todos.filter(todo => todo.completed === filter);
};