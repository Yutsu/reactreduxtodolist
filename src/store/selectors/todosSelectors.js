export const todosSelectors = ({todos}) => {
    return todos.filter(todo => (
        todo.completed === false
    ));
}