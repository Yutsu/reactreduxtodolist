import { createStore, combineReducers } from 'redux';
import filterReducer from './reducers/filterReducer';
import todosReducer from './reducers/todosReducer';
import { saveToLocalStorage, loadFromLocalStorage } from './localStorage';

const rootReducer = combineReducers({
    todos: todosReducer,
    filter: filterReducer
});
const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__()
);

store.subscribe(() => saveToLocalStorage(store.getState()));

export default store;