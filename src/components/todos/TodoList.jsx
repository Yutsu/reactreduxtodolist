import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteTodoAction, updateTodoAction } from '../../store/actions/todosActions';
import { ImCross } from 'react-icons/im';
import { filterSelectors } from '../../store/selectors/filterSelectors';

const Todo = ({todos, onUpdate, onDelete}) => {
    return <ul>
        {todos.length === 0 && <p className="no-todo"> no todo</p>}
        {todos.map(todo => (
            <li key={todo.id} className="todolist">
                <label>
                    <input
                        type="checkbox"
                        checked={todo.completed}
                        onChange={() => onUpdate(todo)}
                    />
                    <p className={todo.completed ? "checked" : ""}>
                        {todo.title}
                    </p>
                </label>
                <button onClick={() => onDelete(todo)}>
                    <ImCross />
                </button>
            </li>
        ))}
    </ul>
}

export const TodoList = () => {
    const todos = useSelector(filterSelectors);
    const dispatch = useDispatch();

    const onUpdate = useCallback(todo => {
        dispatch(updateTodoAction(todo))
    }, [dispatch]);

    const onDelete = useCallback(todo => {
        dispatch(deleteTodoAction(todo))
    }, [dispatch])

    return (
        <Todo
            todos={todos}
            onUpdate={onUpdate}
            onDelete={onDelete} />
    );
};