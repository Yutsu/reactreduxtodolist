import { useEffect } from 'react';
import { connect } from 'react-redux';
import { filterActions } from '../../store/actions/filterActions';
import { deleteClearCompletedAction } from '../../store/actions/todosActions';
import { filterSelectors } from '../../store/selectors/filterSelectors';
import { todosSelectors } from '../../store/selectors/todosSelectors';

export const TodoFilter = ({value, onChange, onDelete, todos}) => {
    useEffect(() => {
        let filters = document.querySelectorAll('.btn-filter');
        filters.forEach((filter) => {
            filter.addEventListener('click', () => {
                if(filter.classList.contains('active')){
                    return;
                } else {
                    filter.classList.add('active');
                }

                let index = filter.getAttribute('data-set');
                Array.from(filters).map(refilter => {
                    if (refilter.getAttribute('data-set') !== index) {
                        refilter.classList.remove('active');
                    }
                    return null;
                })
            })
        })
    }, []);

    return <div className="container-filter">
        <div>
            <p>
                {todos.length}
                {todos.length === 0 || todos.length === 1 ?
                <> item</> : <> items</> } left
            </p>
        </div>
        <div className="container-filter-btn">
            <button disabled={value === null} className="btn-filter active" data-set="1" onClick={()=> onChange(null)}>All</button>
            <button disabled={value === false} className="btn-filter" data-set="2" onClick={()=> onChange(false)}>Active</button>
            <button disabled={value === true} className="btn-filter" data-set="3" onClick={()=> onChange(true)}>Completed</button>
        </div>
            <button disabled={value === null} onClick={() => onDelete()}>Clear completed</button>
    </div>
}

export const TodoFilterStore = connect(
    state => ({
        value: filterSelectors(state),
        todos: todosSelectors(state)
    }),
    dispatch => ({
        onChange: (value) => dispatch(filterActions(value)),
        onDelete: () => dispatch(deleteClearCompletedAction())
    })
)(TodoFilter)
