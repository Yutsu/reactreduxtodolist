import React, { useRef } from 'react';
import { useDispatch } from 'react-redux';
import { addTodoAction } from '../../store/actions/todosActions';

const TodoAddForm = () => {
    const input = useRef(null);
    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        let valueInput = input.current.value;
        if(valueInput) {
            dispatch(addTodoAction(valueInput));
            input.current.value = "";
        }
        input.current.focus();
    }


    return (
        <form onSubmit={handleSubmit}>
            <div className="container-todo-input">
                <input
                    type="text"
                    className="todo-input"
                    placeholder="Ask to do"
                    ref={input}
                />
            </div>
        </form>
    );
};

export default TodoAddForm;