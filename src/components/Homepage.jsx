import React, { useState, useEffect } from 'react';
import { FaMoon, FaSun } from 'react-icons/fa';
import TodoAddForm from './todos/TodoAddForm';
import {TodoFilterStore} from './todos/TodoFilter';
import { TodoList } from './todos/TodoList';

const Homepage = () => {
    const [modeTheme, setModeTheme] = useState(false);
    const changeTheme = () => {
        setModeTheme(!modeTheme);
    }

    const dark = "hsl(235, 21%, 11%)";
    const light = "hsl(236, 33%, 92%)";

    useEffect(() => {
        (modeTheme) ?
        document.body.style.backgroundColor = light :
        document.body.style.backgroundColor = dark
    }, [modeTheme])

    return (
        <div className= {modeTheme ? "container-todo light" : "container-todo"}>
            <div className="background-img"></div>
            <div className="background-img-mobile"></div>
            <div className="container">
                <div className="todo-header">
                    <h1>Todo</h1>
                    <div
                        onClick={changeTheme}
                        className="moon"
                    >
                        {modeTheme && <FaMoon />}
                        {!modeTheme && <FaSun />}
                    </div>
                </div>
                <TodoAddForm />
                <TodoList />
                <TodoFilterStore />
            </div>
        </div>
    );
};

export default Homepage;